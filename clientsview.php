<?php 

  $conn = new PDO("mysql:host=localhost; dbname=testproject", 'testuser', 'testpassword');
  $conn->exec("SET CHARACTER SET utf8");      // Sets encoding UTF-8
  $sql = "SELECT * FROM `clients` ";
  $result = $conn->query($sql);    
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clients View</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <h1>Clients</h1>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Address</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>
          <?php 
            foreach($result as $row) {
          ?>
          <tr>
          <th scope="row"><?php echo ($row['id']); ?></th>
          <td><?php echo ($row['first_name']); ?></td>
          <td><?php echo ($row['last_name']); ?></td>
          <td><?php echo ($row['address']); ?></td>
          <td><?php echo ($row['email']); ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    
    
    
    
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

<?php $conn = null;        // Disconnect ?>